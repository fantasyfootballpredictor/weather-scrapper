# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from bs4 import BeautifulSoup
import requests
import re
import pandas as pd
from itertools import chain


def createId(x):
    year = str(x)
    data = pd.read_csv('{}_weather_data.csv'.format(year))
    l = []
    for i in data.index:
        j = i + 1
        if j <= 9:
            pk = '00' + str(j) + str(year[-2:])
        elif j > 9 and j <= 99:
            pk = '0' + str(j) + str(year[-2:])
        else:
            pk = str(j) + str(year[-2:])
        l.append(pk)
    data['game_id'] = l
    data.to_csv('{}_weather_data.csv'.format(year))

def deleteUnnamed(x):
    year = str(x)
    data = pd.read_csv('{}_weather_data.csv'.format(year))
    data2 = data.loc[:, ~data.columns.str.startswith('Unnamed')]
    data2.to_csv('{}_weather_data.csv'.format(year))


def createSeason(x):
    year = x
    data = pd.read_csv('{}_weather_data.csv'.format(year))
    l = []
    for i in data['week']:
        if i < 9:
            x = 0
            l.append(x)
        else:
            x = 1
            l.append(x)
    data['late_season'] = l
    data.to_csv('{}_weather_data.csv'.format(year))

def pmConvert24(x):
    'Converts string from 12 hour clock to 24. Returns date and 24 kickoff time.'
    dateTime = x.split(' ', 2)
    time12 = re.sub('[^0-9]','', dateTime[2])
    time24 = int(time12) + 1200
    return (dateTime[1], time24)

def amConvert24(x):
    'Converts string from 12 hour clock to 24. Returns date and 24 kickoff time.'
    dateTime = x.split(' ', 2)
    time24 = re.sub('[^0-9]','', dateTime[2])
    return (dateTime[1], time24)

def cleanTeams(l2):
    'Parses list for team names and returns list of teams'
    l3 = []
    for i in l2:
        final = re.match('^Final(.*)', i)
        temperature = re.match('-?\d+\w (.*)', i) #Matches wind and forecast temperature
        network = re.match('[A-Z ]+$', i)
        pm = re.match('(.*) PM$', i)
        am = re.match('(.*) AM$', i)
        forecast = re.match("^Forecast TBD$", i)
        num = re.match("(.*)f (.*)", i)
        if (i == 'Details') or num or temperature or final or am or pm or forecast or network or (i == ''):
            continue
        else:
            l3.append(i)
    return l3

def getTeams(url):
    'Returns a list of teams for one week'
    page2 = requests.get(url)
    soup2 = BeautifulSoup(page2.text, 'lxml')
    div = soup2.find("tbody")
    td = div.find_all("td", class_="text-center")
    l2 = []
    for i in td:
        temp2 = i.get_text()
        l2.append(temp2.strip())
    l3 = cleanTeams(l2)
    return l3

def convertType(x):
    'Returns the Stadium Type as a binary value'
    try:
        if x == "Dome":
            x = 0
        elif x == "Open":
            x = 1
    except:
        print("Stadium Type Error: Value is not 'Dome' or 'Open'")
    return x

def convertWindDirection(x):
    'Returns encoded wind direction'
    directionDict = {'North' : 1, 'North - NorthEast' : 2, 'NorthEast' : 3, 'East - NorthEast' : 4,
                    'East' : 5, 'East - SouthEast' : 6, 'SouthEast' : 7, 'South - SouthEast' : 8,
                    'South' : 9, 'South - SouthWest' : 10, 'SouthWest' : 11, 'West - SouthWest' : 12,
                    'West' : 13, 'West - NorthWest' : 14, 'NorthWest' : 15, 'North - NorthWest' : 16}
    return directionDict.get(x, 'Not Valid')

def convertForecast(x):
    'Returns enocded forecast'
    forecastDict = {'Clear' : 1, 'Partly Cloudy' : 2, 'Mostly Cloudy' : 3, 'Overcast' : 4,
                    'Humid' : 5, 'Humid and Partly Cloudy' : 6, 'Humid and Mostly Cloudy' : 7,
                    'Humid and Overcast' : 8, 'Light Rain' : 9, 'Drizzle' : 10, 'Possible Light Rain' : 11,
                    'Foggy' : 12, 'Windy and Partly Cloudy' : 13, 'Possible Drizzle' : 14, 'Light Sleet' : 15, 'Rain' : 16,
                    'Light Snow' : 17, 'Breezy and Humid' : 18, 'Snow' : 19, 'Heavy Rain' : 20, 'Dry and Partly Cloudy' : 21,
                    'Dry' : 22, 'Flurries' : 23, 'A Few Clouds' : 24, 'Fair' : 25, 'Fog/Mist' : 26, 'Light Rain Fog/Mist' : 27,
                    'Overcast with Haze' : 28, 'Rain Fog/Mist' : 29, 'Partly Sunny' : 30, 'Overcast and Breezy' : 31, 'Overcast and Windy' : 32,
                    'Light Snow Fog/Mist' : 33, 'Fog' : 34, 'Thunderstorm' : 35, 'Sunny' : 36, 'Showers in Vicinity' : 37, 'Light Drizzle Fog/Mist' : 38,
                    'Partly Cloudy and Breezy' : 39, 'NA' : 'Not Valid', 'Mostly Clear' : 40, 'Cloudy' : 41, 'Fair and Breezy' : 42, 'Mostly Cloudy and Breezy' : 43,
                    'Mod Rain, Fog' : 44, 'Thunderstorm Heavy Rain Fog/Mist' : 45, 'Mostly Sunny' : 46, 'Chance Showers' : 47, 'Thunderstorm Rain Fog/Mist' : 48,
                    'Thunder, Lt Rain' : 49, 'Breezy' : 50, 'Snow and Breezy' : 51, 'Fair with Haze' : 52, 'Light Freezing Rain' : 53, 'Heavy Snow Freezing Fog' : 54,
                    'Heavy Rain Fog/Mist' : 55, 'Showers': 56, 'Chance Thunderstorms' : 57, 'Thunderstorm in Vicinity Light Rain Fog/Mist' : 58, 'Light Drizzle' : 59,
                    'Rain Fog/Mist and Breezy' : 60, 'Mostly Cloudy and Windy' : 61, 'Slight Chance Rain Showers' : 62, 'Light Rain and Breezy' : 63, 'Shallow Fog' : 64,
                    'Fair and Windy' : 65, 'Blizzard' : 66, 'Decreasing Clouds' : 67}
    stadiumDict = {'Soldier Field' : 1, 'Bank of America Stadium' : 2, 'Lincoln Financial Field' : 3, 'MetLife Stadium' : 4, 'U.S. Bank Stadium' : 5,
                   'Hard Rock Stadium' : 6, 'TIAA Bank Field' : 7, 'FirstEnergy Stadium' : 8, 'ROKiT Field at Dignity Health Sports Park' : 9,
                   'CenturyLink Field' : 10, 'Raymond James Stadium' : 11, 'AT&T Stadium' : 12, 'State Farm Stadium' : 13, 'Gillette Stadium' : 14,
                   'Mercedes-Benz Superdome' : 15, 'Oakland–Alameda County Coliseum' : 16, 'M&T Bank Stadium' : 17, 'FedExField' : 18, 'Nissan Stadium' : 19,
                   'Heinz Field' : 20, 'Paul Brown Stadium' : 21, 'Ford Field' : 22, 'Lambeau Field' : 23, 'NRG Stadium' : 24, 'Los Angeles Memorial Coliseum' : 25,
                   'Broncos Stadium at Mile High' : 26, 'Mercedes-Benz Stadium' : 27, 'New Era Field' : 28, 'Arrowhead Stadium' : 29, 'Lucas Oil Stadium' : 30,
                   "Levi's Stadium" : 31, 'Tottenham Hotspur Stadium' : 32, 'Wembley Stadium' : 33, 'Estadio Azteca (Mexico City)' : 34, 'Camping World Stadium' : 35,
                   'Tom Benson Hall of Fame Stadium' : 36, 'Allegiant Stadium' : 39, 'SoFi Stadium' : 40, 'Georgia Dome': 41, 'Qualcomm Stadium' : 42, 'Edward Jones Dome' : 43,
                   'TCF Bank Stadium' : 44, 'Twickenham Stadium' : 45, 'Candlestick Park' : 46, 'Hubert H. Humphrey Metrodome' : 47, 'Rogers Centre' : 48, 'Cleveland Municipal Stadium' : 49}
    if re.match('(.*)Few Clouds(.*)', x, flags=re.IGNORECASE):
        x = 'A Few Clouds'
    elif x == 'Lt Rain':
        x = 'Light Rain'
    elif x == 'Areas Drizzle':
        x = 'Drizzle'
    elif x == 'Partly Cloudy with Haze':
        x = 'Partly Cloudy'
    elif x == 'Patchy Fog':
        x = 'Fog'
    elif x == 'Thunderstorm Light Rain':
        x = 'Thunderstorm'
    elif x == 'Rain Showers':
        x = 'Showers'
    if forecastDict.get(x) == None:
        return stadiumDict.get(x, 'Not Valid')
    else:
        return forecastDict.get(x, 'Not Valid')

def convertSurface(x):
    'Returns encoded Surface Type'
    sufaceDict = {'Grass' : 1, 'Desso GrassMaster' : 2, 'Fieldturf' : 3, 'UBU Sports Speed Series S5-M' : 4, '419 Tifway Bermuda Grass' : 5,
                  'Kentucky Bluegrass' : 6, 'RealGrass Matrix' : 7, 'Bluegrass' : 8, 'Desso Grassmaster' : 9, 'Natural grass' : 10, 'FieldTurf Revolution 360' : 11,
                  'A-Turf Titan' : 12, 'Bermuda Bandera / Perennial Ryegrass mixture' : 13}
    if x == 'grass':
        x = 'Grass'
    elif x == 'FieldTurf':
        x = 'Fieldturf'
    return sufaceDict.get(x, 'Not Valid')

def convertTeams(x):
    'Returns list of: week #, teams, and encoded teams'
    teamsDict = {'Cardinals' : ['ARI', 1], 'Falcons' : ['ATL', 2], 'Ravens' : ['BAL', 3], 'Bills' : ['BUF', 4], 'Panthers' : ['CAR', 5], 'Bears' : ['CHI', 6],
                 'Bengals' : ['CIN', 7], 'Browns' : ['CLE', 8], 'Cowboys' : ['DAL', 9], 'Broncos' : ['DEN', 10], 'Lions' : ['DET', 11], 'Packers' : ['GB', 12],
                 'Texans' : ['HOU', 13], 'Colts' : ['IND', 14], 'Jaguars' : ['JAX', 15], 'Chiefs' : ['KC', 16], 'Chargers' : ['LAC', 17], 'Rams' : ['LAR', 18],
                 'Dolphins' : ['MIA', 19], 'Vikings' : ['MIN', 20], 'Patriots' : ['NE', 21], 'Saints' : ['NO', 22], 'Giants' : ['NYG', 23], 'Jets' : ['NYJ', 24],
                 'Raiders' : ['OAK', 25], 'Eagles' : ['PHI', 26], 'Steelers' : ['PIT', 27], 'Seahawks' : ['SEA', 28], '49ers' : ['SF', 29], 'Buccaneers' : ['TB', 30],
                 'Titans' : ['TEN', 31], 'Redskins' : ['WAS', 32]}
    temp = x.split(' ')
    l = []
    l.append(temp[2])
    teamA = teamsDict.get(temp[4])
    teamH = teamsDict.get(temp[6])
    l.append(teamA[0])
    l.append(teamA[1])
    l.append(teamH[0])
    l.append(teamH[1])
    return l

def convertTemp(x, convert = True):
    if convert == True:
        if re.match("(.*)[0-9]+/[0-9]+", x):
            x = int(x[-2:])
        else:
            x = int(x)
        if x > 75:
            x = 5
        elif x > 60 and x <= 75:
            x = 4
        elif x > 45 and x <= 60:
            x = 3
        elif x > 30 and x <= 45:
            x = 2
        else:
            x = 1
    else:
        return x
    return x

def convertSpeed(x, convert = True):
    if convert == True:
        if x > 15:
            x = 4
        elif x > 10 and x <= 15:
            x = 3
        elif x > 5 and x <= 10:
            x = 2
        elif x <= 5:
            x = 1
        return x
    else:
        return x

def convertHumidity(x, convert = True):
    if convert == True:
        if x >= 81:
            x = 3
        elif x >= 41 and x <= 80:
            x = 2
        elif x > 0 and x <= 40:
            x = 1
    else:
        return x
    return x

def convertCCover(x, convert = True):
    if convert == True:
        if x >= 67 and x <= 100:
            x = 3
        elif x >= 34 and x <= 66:
            x = 2
        elif x > 0 and x <= 33:
            x = 1
    else:
        return x
    return x

def getGameInfo(url):
    'Returns list of all raw weather data concerning one game. Includes team names and week as well'
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'lxml')
    data = soup.find_all('p')
    rawWeek = soup.find("h1")
    l = []
    l.append(rawWeek.get_text())
    for i in data:
        temp = i.get_text()
        l.append(temp.strip())
    return l

def cleanGame(l):
    'Inputs list of raw game data and returns cleaned dict of one game.'
    weather = []
    del l[14:55] #Deletes Quarters 2 - 4
    del l[-3:] #Deletes liscensing information
    for i in l:
        tmp = re.match("Temperature:(.*)$", i)
        feels = re.match("Feels Like:(.*)$", i)
        wind = re.match("(.*)mi (.*)$", i)
        humd = re.match("Humidity:(.*)$", i)
        cover = re.match("Cloud Cover:(.*)$", i)
        precip = re.match("Precipitation Prob:(.*)", i)
        surf = re.match("Surface:(.*)", i)
        vis = re.match("^Visibility(.*)", i)
        bar = re.match("^Barometer(.*)", i)
        dew = re.match("^Dew Point(.*)", i)
        location = re.match("^Location(.*)", i)
        zipCode = re.match("^Zip(.*)", i)
        cap = re.match("^Capacity(.*)", i)
        stadiumType = re.match("^Type(.*)", i)
        week = re.match("(.*)Weather report(.*)", i)
        if (i == 'Kickoff') or (i == 'Q1') or (i == 'Q2') or (i == 'Q3') or (i == 'Q4') or (i == '') or zipCode or cap or location:
            continue
        elif tmp or feels:
            temp = i.split(':', 1)
            new = convertTemp(temp[1][:-2], False)
            weather.append(new)
        elif dew or vis:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-2]))
        elif wind:
            temp = i.split(': ', 1)
            speed = temp[1].split(' ', 1)
            converted = convertSpeed(int(speed[0][:-2]), False)
            weather.append(converted)
            direction = convertWindDirection(speed[1]) #Maps wind direction
            weather.append(direction)
        elif humd:
            temp = i.split(':', 1)
            humidity = convertHumidity(int(temp[1][:-1]), False)
            weather.append(humidity)
        elif cover:
            temp = i.split(':', 1)
            ccover = convertCCover(int(temp[1][:-1]), False)
            weather.append(ccover)
        elif precip or bar:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-1]))
        elif surf:
            temp = i.split(": ", 1)
            surface = convertSurface(temp[1])
            weather.append(surface)
        elif stadiumType:
            temp = i.split(": ", 1)
            temp = convertType(temp[1]) #Maps stadium type
            weather.append(temp)
        elif week:
            weekTeam = convertTeams(i) #Maps team names and codes
            for i in weekTeam:
                weather.append(i)
        else:
            forecast = convertForecast(i)
            weather.append(forecast)
    keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'feel_ko', 'wind_ko', 'wdirection_ko',
         'humidity_ko', 'visibility_ko', 'barometer_ko', 'dew_ko', 'ccover_ko',
         'precip_prob_ko', 'stadium', 'surface', 'stadium_type']
    weatherDict = dict(zip(keysW, weather))
    return weatherDict

def cleanGame15(l):
    'Inputs list of raw game data and returns cleaned dict of one game; from 2015 and before.'
    weather = []
    del l[-2:] #Deletes liscensing information
    for i in l:
        tmp = re.match("Temperature:(.*)$", i)
        feels = re.match("Feels Like:(.*)$", i)
        wind = re.match("(.*)mi (.*)$", i)
        humd = re.match("Humidity:(.*)$", i)
        cover = re.match("Cloud Cover:(.*)$", i)
        precip = re.match("Precipitation Prob:(.*)", i)
        surf = re.match("Surface:(.*)", i)
        vis = re.match("^Visibility(.*)", i)
        bar = re.match("^Barometer(.*)", i)
        dew = re.match("^Dew Point(.*)", i)
        location = re.match("^Location(.*)", i)
        zipCode = re.match("^Zip(.*)", i)
        cap = re.match("^Capacity(.*)", i)
        stadiumType = re.match("^Type(.*)", i)
        week = re.match("(.*)Weather report(.*)", i)
        if (i == 'Kickoff') or (i == 'Q1') or (i == 'Q2') or (i == 'Q3') or (i == 'Q4') or (i == '') or (i == 'Click to see stadium details') or zipCode or cap or location:
            continue
        elif tmp or feels:
            temp = i.split(':', 1)
            new = convertTemp(int(temp[1][:-2]), False)
            weather.append(new)
        elif dew or vis:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-2]))
        elif wind:
            temp = i.split(': ', 1)
            speed = temp[1].split(' ', 1)
            weather.append(int(speed[0][:-2]))
            direction = convertWindDirection(speed[1], False) #Maps wind direction
            weather.append(direction)
        elif humd or cover or precip or bar:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-1]))
        elif surf:
            temp = i.split(": ", 1)
            surface = convertSurface(temp[1], False)
            weather.append(surface)
        elif stadiumType:
            temp = i.split(": ", 1)
            temp = convertType(temp[1]) #Maps stadium type
            weather.append(temp)
        elif week:
            weekTeam = convertTeams(i) #Maps team names and codes
            for i in weekTeam:
                weather.append(i)
        else:
            forecast = convertForecast(i)
            weather.append(forecast)
    keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'feel_ko', 'wind_ko', 'wdirection_ko',
         'humidity_ko', 'visibility_ko', 'barometer_ko', 'dew_ko', 'ccover_ko',
         'precip_prob_ko', 'stadium', 'surface', 'stadium_type']
    weatherDict = dict(zip(keysW, weather))
    return weatherDict

def cleanGame09(l):
    'Inputs list of raw game data and returns cleaned dict of one game; from 2015 and before.'
    weather = []
    del l[-2:] #Deletes liscensing information
    for i in l:
        tmp = re.match("Temperature:(.*)$", i)
        feels = re.match("Feels Like:(.*)$", i)
        wind = re.match("(.*)mi (.*)$", i)
        humd = re.match("Humidity:(.*)$", i)
        cover = re.match("Cloud Cover:(.*)$", i)
        precip = re.match("Precipitation Prob:(.*)", i)
        surf = re.match("Surface:(.*)", i)
        vis = re.match("^Visibility(.*)", i)
        bar = re.match("^Barometer(.*)", i)
        dew = re.match("^Dew Point(.*)", i)
        location = re.match("^Location(.*)", i)
        zipCode = re.match("^Zip(.*)", i)
        cap = re.match("^Capacity(.*)", i)
        stadiumType = re.match("^Type(.*)", i)
        week = re.match("(.*)Weather report(.*)", i)
        if (i == 'Kickoff') or (i == 'Q1') or (i == 'Q2') or (i == 'Q3') or (i == 'Q4') or (i == '') or (i == 'Click to see stadium details') or zipCode or cap or location:
            continue
        elif tmp or feels or dew or vis:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-2]))
        elif wind:
            temp = i.split(': ', 1)
            speed = temp[1].split(' ', 1)
            weather.append(int(speed[0][:-2]))
            direction = convertWindDirection(speed[1]) #Maps wind direction
            weather.append(direction)
        elif humd or cover or precip or bar:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-1]))
        elif surf:
            temp = i.split(": ", 1)
            surface = convertSurface(temp[1])
            weather.append(surface)
        elif stadiumType:
            temp = i.split(": ", 1)
            temp = convertType(temp[1]) #Maps stadium type
            weather.append(temp)
        elif week:
            weekTeam = convertTeams(i) #Maps team names and codes
            for i in weekTeam:
                weather.append(i)
        else:
            forecast = convertForecast(i)
            weather.append(forecast)
    keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'humidity_ko', 'visibility_ko',
             'barometer_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
    weatherDict = dict(zip(keysW, weather))
    return weatherDict

def cleanGame14(l):
    'Inputs list of raw game data and returns cleaned dict of one game; from 2015 and before.'
    weather = []
    del l[-2:] #Deletes liscensing information
    for i in l:
        tmp = re.match("Temperature:(.*)$", i)
        feels = re.match("Feels Like:(.*)$", i)
        wind = re.match("(.*)mi (.*)$", i)
        humd = re.match("Humidity:(.*)$", i)
        cover = re.match("Cloud Cover:(.*)$", i)
        precip = re.match("Precipitation Prob:(.*)", i)
        surf = re.match("Surface:(.*)", i)
        vis = re.match("^Visibility(.*)", i)
        bar = re.match("^Barometer(.*)", i)
        dew = re.match("^Dew Point(.*)", i)
        location = re.match("^Location(.*)", i)
        zipCode = re.match("^Zip(.*)", i)
        cap = re.match("^Capacity(.*)", i)
        stadiumType = re.match("^Type(.*)", i)
        week = re.match("(.*)Weather report(.*)", i)
        if (i == 'Kickoff') or (i == 'Q1') or (i == 'Q2') or (i == 'Q3') or (i == 'Q4') or (i == '') or (i == 'Click to see stadium details') or zipCode or cap or location:
            continue
        elif tmp or feels:
            temp = i.split(':', 1)
            new = convertTemp(temp[1][:-2], False)
            weather.append(new)
        elif dew or vis:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-2]))
        elif wind:
            temp = i.split(': ', 1)
            speed = temp[1].split(' ', 1)
            converted = convertSpeed(int(speed[0][:-2]), False)
            weather.append(converted)
            direction = convertWindDirection(speed[1]) #Maps wind direction
            weather.append(direction)
        elif humd:
            temp = i.split(':', 1)
            humidity = convertHumidity(int(temp[1][:-1]), False)
            weather.append(humidity)
        elif cover:
            temp = i.split(':', 1)
            ccover = convertCCover(int(temp[1][:-1]), False)
            weather.append(ccover)
        elif precip or bar:
            temp = i.split(':', 1)
            weather.append(int(temp[1][:-1]))
        elif surf:
            temp = i.split(": ", 1)
            surface = convertSurface(temp[1])
            weather.append(surface)
        elif stadiumType:
            temp = i.split(": ", 1)
            temp = convertType(temp[1]) #Maps stadium type
            weather.append(temp)
        elif week:
            weekTeam = convertTeams(i) #Maps team names and codes
            for i in weekTeam:
                weather.append(i)
        else:
            forecast = convertForecast(i)
            weather.append(forecast)
    if len(weather) == 19:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'feel_ko', 'temp_ko', 'wind_ko', 'wdirection_ko', 'humidity_ko', 'visibility_ko',
             'barometer_ko', 'dew_ko', 'ccover_ko', 'precip_prob_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
        if (None in weatherDict.values() == True):
            print('Error in dict:\n', weatherDict)
            print(weatherDict)
    elif len(weather) == 10:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
    elif len(weather) == 14:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'humidity_ko', 'visibility_ko',
             'barometer_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
    elif len(weather) == 12:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'humidity_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
    elif len(weather) == 13:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'humidity_ko', 'visibility_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
    elif len(weather) == 14:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'humidity_ko', 'visibility_ko', 'barometer_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
    elif len(weather) == 15:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'feel_ko', 'humidity_ko', 'visibility_ko', 'barometer_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
    else:
        keysW = ['week', 'ateam', 'acode', 'hteam', 'hcode', 'weather_ko', 'temp_ko', 'wind_ko', 'wdirection_ko', 'humidity_ko', 'visibility_ko',
             'barometer_ko', 'dew_ko', 'stadium', 'surface', 'stadium_type']
        weatherDict = dict(zip(keysW, weather))
        if (None in weatherDict.values() == True):
            print('Error in dict:\n')
            print(weatherDict)
    return weatherDict

def getDate(url):
    'Returns a dict with the date, time, and score of one game'
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'lxml')
    data2 = soup.find_all("strong") #Gets score and kickoff date and time
    details = []
    for i in data2:
        temp = i.get_text()
        pm = re.match("(.*)PM(.*)", temp)
        am = re.match("(.*)AM(.*)", temp)
        if pm:
            temp = pmConvert24(temp) #Converts time to 24 hour clock
            details.append(temp[0])
            details.append(temp[1])
        elif am:
            temp = amConvert24(temp) #Converts time to 24 hour clock
            details.append(temp[0])
            details.append(temp[1])
        elif temp == 'Final':
            continue
        else:
            details.append(temp)
    keysT = ['date', 'ko_time', 'ascored', 'hscored']
    detailsDict = dict(zip(keysT, details))
    return detailsDict

def main2019():
    'Scrapes nflweather.com for all weather details for each game in the 2019 season and exports a csv'
    url = "http://nflweather.com/en/week/2019/week-{}/"
    url2 = "http://nflweather.com/en/game/2019/week-{}/{}-at-{}"
    gamesYear = []
    for week in range(17):
        link = url.format(week+1)
        teams = getTeams(link)
        j = 0
        gamesWeek = []
        for i in range(len(teams)//2):
            linkDetails = url2.format(week+1, teams[j], teams[j+1])
            timeDict = getDate(linkDetails)
            rawInfo = getGameInfo(linkDetails)
            gameDict = cleanGame(rawInfo)
            timeDict.update(gameDict)
            gamesWeek.append(timeDict)
            j += 2
        gamesYear.append(gamesWeek)
    season = pd.DataFrame(list(chain.from_iterable(gamesYear)))
    season.to_csv('2019_weather_data.csv') 
    #Don't forget about createSeason and createId
    createId(year)
    createSeason(year)
    deleteUnnamed(year)

def main2015(year):
    'Scrapes nflweather.com for all weather details for each game in the 2019 season and exports a csv'
    if year == 2010:
        url = "http://nflweather.com/en/week/{}/week-{}-2"
        url2 = "http://nflweather.com/en/game/{}/week-{}-2/{}-at-{}"
    else:
        url = "http://nflweather.com/en/week/{}/week-{}/"
        url2 = "http://nflweather.com/en/game/{}/week-{}/{}-at-{}"
    gamesYear = []
    gamesWeek = []
    for week in range(17):
        link = url.format(year, week+1)
        teams = getTeams(link)
        j = 0
        try:
            for i in range(len(teams)//2):
                linkDetails = url2.format(year, week+1, teams[j], teams[j+1])
                timeDict = getDate(linkDetails)
                rawInfo = getGameInfo(linkDetails)
                gameDict = cleanGame14(rawInfo)
                timeDict.update(gameDict)
                gamesWeek.append(timeDict)
                j += 2
        except:
            print(linkDetails)
    gamesYear.append(gamesWeek)
    season = pd.DataFrame(list(chain.from_iterable(gamesYear)))
    season.to_csv('{}_weather_data.csv'.format(year)) 
    #Don't forget about createSeason and createId
    createId(year)
    createSeason(year)
    deleteUnnamed(year)



for i in range(4):
    year = i + 2017

    main2015(year)

#Different Tests to make sure the data is correct
#season.loc[season['ateam'] != 16]

#season.loc[season['stadium_type'] != 1, ['week', 'hteam']]

#season.loc[season['stadium_type'] == 'Not Valid', ['week', 'hteam']]
#season.loc[season['stadium'] == 'Not Valid', ['week', 'hteam']]

#season.loc[season['surface'] >= 14, ['week', 'hteam']]

#season.loc[season['stadium'] == 'nan', ['week', 'hteam']]
#season.loc[season['weather_ko'] == 'Not Valid', ['week', 'hteam']]

#week = 9
#year = 2019
#link = 'http://nflweather.com/en/week/2014/Week-10/'

#linkDetails = 'http://nflweather.com/en/game/2009/week-16/vikings-at-bears'
#linkDetails3 = 'http://nflweather.com/en/game/2014/Week-14/cowboys-at-bears'

#data = pd.read_csv('2019_weather_data.csv')
#data['hteam'].value_counts()
